<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>

<head>
	<title>Company Home Page</title>
</head>

<body>
	<h2>Company Home Page</h2>
	<hr>

	<p>
		Welcome to the home page!
	</p>

	<hr>

		User: <security:authentication property="principal.username"/>
		<br><br>
		Role(s): <security:authentication property="principal.authorities"/>
	<hr>

	<security:authorize access="hasRole('MANAGER')">
	<!-- Link to point to /managers (only for employees with the role of MANAGER -->
	<p>
		<a href="${pageContext.request.contextPath}/managers">Managers Meeting</a>
		(Only for Managers)
	</p>
	</security:authorize>

		<br>

	<security:authorize access="hasRole('ADMINISTRATOR')">
	<!-- Link to point to /administrator (only for employees with the role of ADMINISTRATOR -->
	<p>
		<a href="${pageContext.request.contextPath}/administrators">Administrators meeting</a>
		(Only for Administrators)

	</p>

	</security:authorize>

	<form:form action="${pageContext.request.contextPath}/logout"
			   method="post">

		<input type="submit" value="Logout"/>

	</form:form>

</body>

</html>