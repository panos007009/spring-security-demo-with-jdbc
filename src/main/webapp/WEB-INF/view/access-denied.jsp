<%--
  Created by IntelliJ IDEA.
  User: PANOS
  Date: 01/03/2021
  Time: 14:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Company - Access Denied</title>
</head>

<body>

    <h2>Access Denied - You are not authorized to access this page´s resources and information</h2>

    <hr>

    <a href="${pageContext.request.contextPath}/">Back to home page</a>

</body>

</html>
